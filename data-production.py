import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import glob
import os 

file_data = 'dataset/'
master_data = sorted(glob.glob(file_data + '*'))

list_file_errors = []
list_file_success = []
cek_std = {}
plot_nT = {}
list_jarak_data_dari_mean = {}
hasil = []
list_nama_file = []
list_waktu = {}
hist_std = []
for no in range(len(master_data)):
    no_file = no
    try:
        data = pd.read_csv(master_data[no_file], sep=" ", header=1)
        list_nama_file.append(master_data[no_file])
        list_kolom = data.columns
        for idx in range(3, len(list_kolom)):
            data.drop([list_kolom[idx]], axis=1, inplace=True)
        data = data.iloc[3:, :]
        data.drop(['1'], axis=1, inplace=True)
        data.columns = ['time', 'nT']
        data['nT'] = data['nT'].astype(float)
        out = data['time'].astype(float).astype(int).values
        data_time = []
        for i in out:
            h = str(i)[0]
            mm = str(i)[1:3]
            ss = str(i)[3:]
            new_time = h + '-' + mm + '-' + ss
            data_time.append(new_time)
        data.drop(['time'], axis=1, inplace=True)
        data['time'] = data_time
        data.reset_index(inplace=True)
        data.drop(['index'], axis=1, inplace=True)
        data_mean = data.describe().loc['mean'].values[0]
        data_std = data.describe().loc['std'].values[0]
        list_file_success.append(master_data[no_file])
        plot_nT.update({master_data[no_file] : list(data['nT'])})
        cek_std.update({master_data[no_file] : round(data_std, 2)})
        save_mean = []
        filtered = []
        for j in data['nT'].values:
            res = abs(j - data_mean)
            res = np.round(res, 2)
            save_mean.append(res) 
            if res == 2.5:
                filtered.append(np.nan)
            else:
                filtered.append(j)
        new_filtered = []
        for x in save_mean:
            if x == max(save_mean):
                new_filtered.append(np.nan)
            else:
                new_filtered.append(x)
        
#         data['filtered'] = new_filtered
#         filtered_std = data['filtered'].std()
#         num_after_filtered = len(data['filtered'].values) - data['filtered'].isnull().sum()
#         out = np.squeeze(save_mean)
#         list_jarak_data_dari_mean.update({'jarak data {} dari mean'.format(master_data[no_file]):out})
#         list_waktu.update({'{}'.format(master_data[no_file]) : data['time'].values})
        
        data_bef = data.copy()
        
        print('[file ke-{}]\n'.format(no))
        print('jumlah data sebelum : \n', len(data_bef['nT'].values))
        print('data nT sebelum filter : \n', data_bef['nT'].values)
        print('std sebelum filter : \n', data_std)
        data_std_new = data_std
        while data_std_new > 2.0:    
            his = data_std_new
            idx = save_mean.index(max(save_mean))
            del data['nT'][idx], save_mean[idx]
            new_std = pd.DataFrame(data['nT'].values).std()[0]
            data_std_new = data_std_new - new_std
            hist_std.append(data_std_new)
            print('std setelah filter : \n',data_std_new)
            print('data setelah filter : \n', data['nT'].values)
        
        print('nama file : \n', master_data[no_file])
        print('jumlah data setelah filter : \n', len(data['nT'].values))
        print()
        
        nama_file = master_data[no_file].split('/')[-1].split('.')[0]
        if os.path.exists('output/data_preprocessed') is False:
            os.makedirs('output/data_preprocessed')
        else:
            pass 
        data.to_excel('output/data_preprocessed/{}.xlsx'.format(nama_file),sheet_name=nama_file)
        try:
            meta_data = pd.DataFrame({
                'jumlah_data_sebelum_filter':[len(data_bef['nT'].values)],
                'std_sebelum_filter':[data_std],
                'jumlah_data_setelah_std':[len(data['nT'].values)],
                'std_setelah_filter':[data_std_new]
            })
        except:
            raise
        
        if os.path.exists('output/meta_data') is False:
            os.makedirs('output/meta_data')
    
        meta_data.to_csv('output/meta_data/meta_data_{}.csv'.format(nama_file), index=False)
    
    except:
        raise
        list_file_errors.append(master_data[no_file])
        print('gagal')
        print('file {} error, mohon samakan pola format datanya'.format(master_data[no_file]))
        print()
