from flask import Flask, request, render_template, redirect, url_for, flash
app = Flask(__name__)
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import glob
import os 
import pickle
import platform 

from werkzeug.utils import secure_filename

UPLOAD_FOLDER = '/'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SECRET_KEY'] = 'tugas_akhir'
app.jinja_env.filters['zip'] = zip

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/', methods=['GET', 'POST'])
def uploaded_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file or allowed_file(file.filename):
            filename = secure_filename(file.filename)
            if os.path.exists('upload') is False:
                os.mkdir('upload')

            file.save(os.path.join('upload', filename))

            master_data = [os.path.normcase('upload/{}'.format(filename))]
            list_file_errors = []
            list_file_success = []
            cek_std = {}
            plot_nT = {}
            list_jarak_data_dari_mean = {}
            hasil = []
            list_nama_file = []
            list_waktu = {}
            hist_std = []
            no_file = 0
            try:
                data = pd.read_csv(master_data[no_file], sep=" ", header=1)
                list_nama_file.append(master_data[no_file])
                list_kolom = data.columns
                for idx in range(3, len(list_kolom)):
                    data.drop([list_kolom[idx]], axis=1, inplace=True)
                data = data.iloc[3:, :]
                if data.columns[1] == '1':
                    data.drop(['1'], axis=1, inplace=True)
                else:
                    data = pd.read_csv(master_data[no_file], sep='\t', header=5)
                    list_nama_file.append(master_data[no_file])
                    list_kolom = data.columns
                    for idx in range(2, len(list_kolom)):
                        data.drop([list_kolom[idx]], axis=1, inplace=True)
                    try:
                        data['sq'] = data['sq'].apply(lambda x:x.replace(',', '.'))
                    except:
                        data = pd.read_csv(master_data[no_file], sep=' ',header=4, index_col=1)
                        list_nama_file.append(master_data[no_file])
                        list_kolom = data.columns
                        for idx in range(2, len(list_kolom)):
                            data.drop([list_kolom[idx]], axis=1, inplace=True)
                        data.reset_index(inplace=True)
                        data.drop(['index'], axis=1, inplace=True)

                data.columns = ['time', 'nT']
                data['nT'] = data['nT'].astype(float)
                out = data['time'].astype(float).astype(int).values
                data_time = []
                for i in out:
                    h = str(i)[0]
                    mm = str(i)[1:3]
                    ss = str(i)[3:]
                    new_time = h + '-' + mm + '-' + ss
                    data_time.append(new_time)
                data.drop(['time'], axis=1, inplace=True)
                data['time'] = data_time
                data.reset_index(inplace=True)
                data.drop(['index'], axis=1, inplace=True)
                data_mean = data.describe().loc['mean'].values[0]
                data_std = data.describe().loc['std'].values[0]
                dt_std_file = open('std.pkl', 'wb')
                dt_std = pickle.dump(data_std, dt_std_file)
                dt_std_file.close()
                data_std_sebelum = data_std.copy()
                aku = data_std
                list_file_success.append(master_data[no_file])
                plot_nT.update({master_data[no_file] : list(data['nT'])})
                cek_std.update({master_data[no_file] : round(data_std, 2)})
                save_mean = []
                filtered = []
                for j in data['nT'].values:
                    res = abs(j - data_mean)
                    res = np.round(res, 2)
                    save_mean.append(res) 
                    if res == 2.5:
                        filtered.append(np.nan)
                    else:
                        filtered.append(j)
                new_filtered = []
                for x in save_mean:
                    if x == max(save_mean):
                        new_filtered.append(np.nan)
                    else:
                        new_filtered.append(x)

                data_bef = data.copy()
                print()
                print('[file ke-{}]'.format(no_file))
                print('jumlah data sebelum : \n', len(data_bef['nT'].values))
                print('std sebelum filter : \n', data_std)

                hist_std = []
                data_     = data['nT'].values.tolist()
                time_     = data['time'].values.tolist()
                data_std = data['nT'].std()
                rata = data['nT'].mean()
                save_mean = data.assign(meanx=lambda x : round(abs(rata - x.nT), 5))
                save_mean = save_mean['meanx'].tolist()
                data_rem = []
                hapus_hist_idx  = []
                output = []
                tm = []
                std_std = [0]
                save_mean_fix = pd.DataFrame(save_mean).values.flatten().tolist()
                data_fix = pd.DataFrame(data_).values.flatten().tolist()
                time_fix = pd.DataFrame(time_).values.flatten().tolist()
                while data_std > 2.0:    
                    his = data_std
                    idx = save_mean.index(max(save_mean))
                    data_rem.append(data_[idx])
                    tm.append(time_[idx])
                    hapus_hist_idx.append(idx)
                    data_fix[idx] = np.nan
                    save_mean_fix[idx] = np.nan 
                    time_fix[idx] = np.nan
                    del data_[idx], save_mean[idx], time_[idx]
                    data_std = pd.DataFrame(data_).std()[0]
                    output.append(data_std)
                    
                    # if len(save_mean) == 30:
                    #     break
                print('n_save_mean_aft : \n', len(save_mean))
                print('data_std : \n', data_std)
                print('index data yang dihapus : \n', hapus_hist_idx)
                print('time data yang dihapus : \n', tm)
                print('data yang dihapus : \n', data_rem)
                print('data std setelah filter : \n',output)

                current_os = platform.system()
                if current_os == 'Linux':
                    nama_file = master_data[no_file].split('/')[-1].split('.')[0]
                else:
                    nama_file = master_data[no_file].split('\\')[-1].split('.')[0]


                if os.path.exists('output/data_preprocessed') is False:
                    os.makedirs('output/data_preprocessed')
                else:
                    pass 
                file_excel = os.path.normcase('output/data_preprocessed/{}_processed.xlsx'.format(nama_file))
                data_out_printed = pd.DataFrame({'Time':time_,'nT':data_})
                data_out_printed.to_excel(file_excel,sheet_name=nama_file)
                try:
                    output = np.round(np.squeeze(output),2)
                    dt_std_load = pickle.load(open('std.pkl', 'rb'))
                    meta_data = pd.DataFrame({
                        'jumlah_data_sebelum_filter':[len(data_bef['nT'].values)],
                        'std_sebelum_filter':[round(dt_std_load,2)],
                        'jumlah_data_setelah_std':[len(data_)],
                        'std_setelah_filter':[output],
                        'index_data_yang_dihapus':[hapus_hist_idx],
                        'data_yang_dihapus':[data_rem],
                        'time_yang_dihapus':[tm]
                    })

                except:
                    raise

                if os.path.exists('output/meta_data') is False:
                    os.makedirs('output/meta_data')
                file_csv = os.path.normcase('output/meta_data/meta_data_{}.csv'.format(nama_file))
                meta_data.to_csv(file_csv, index=False)

                if os.path.isfile(file_csv) and os.path.isfile(file_csv):
                    flash('Data Berhasil Diproses', 'success')

                # final_out = pd.DataFrame(
                #     {'no_data': [hapus_hist_idx], 'time':[tm], 'nT':[data_rem]}
                #     ).sort_values()
                
            except:
                raise
                list_file_errors.append(master_data[no_file])
                print('gagal')
                print('file {} error, mohon samakan pola format datanya'.format(master_data[no_file]))
            
            try:
                output = output[-1]
                return render_template('processing.html', std_before=round(dt_std_load,2), 
                        std_after=output, data_hapus=data_rem, time_hapus=tm, 
                        n_before=len(data_bef['nT'].values), n_after=len(data_), no=np.arange(len(tm)))
            except:
                output = []
                return render_template('processing.html', std_before=round(dt_std_load,2), 
                        std_after=output, data_hapus=data_rem, time_hapus=tm, 
                        n_before=len(data_bef['nT'].values), n_after=len(data_), no=np.arange(len(tm)))

            
            

    return render_template('processing.html')
if __name__ == "__main__":
    app.run(debug=True)



